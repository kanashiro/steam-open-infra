{% set registry_domain = pillar['agent']['config']['registry_domain'] %}

apt-transport-https:
  pkg.installed

docker_repo:
  pkgrepo.managed:
    - humanname: Docker apt repository
    - name: deb [arch=amd64] https://download.docker.com/linux/debian stretch stable
    - file: /etc/apt/sources.list.d/docker.list
    - key_url: https://download.docker.com/linux/debian/gpg

docker-ce:
  pkg:
    - installed
    - refresh: True
    - require:
      - pkgrepo: docker_repo

docker_daemon_config:
  file:
    - managed
    - name: /etc/docker/daemon.json
    - source: salt://agent/config/docker-daemon.json.j2
    - template: jinja
    - context:
        docker_registry_url: {{ registry_domain }}
    - mode: 644
    - user: root
    - group: root
