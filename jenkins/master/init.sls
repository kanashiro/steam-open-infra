default-jre:
  pkg.installed

apt-transport-https:
  pkg.installed

jenkins_apt_key:
  cmd.run:
    - name: wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -

jenkins_apt_repo:
  cmd.run:
    - name: echo 'deb https://pkg.jenkins.io/debian-stable binary/' > /etc/apt/sources.list.d/jenkins.list
    - require:
      - cmd: jenkins_apt_key

jenkins:
  pkg:
    - installed
    - refresh: True
    - require:
      - cmd: jenkins_apt_repo
