openssh-client:
  pkg.installed

generate_ssh_key_jenkins:
  cmd.run:
    - name: ssh-keygen -q -N '' -f /var/lib/jenkins/.ssh/id_rsa
    - runas: jenkins
    - unless: test -f /var/lib/jenkins/.ssh/id_rsa
    - require:
      - pkg: openssh-client

print_jenkins_public_key:
  cmd.run:
    - name: cat /var/lib/jenkins/.ssh/id_rsa.pub
    - require:
      - cmd: generate_ssh_key_jenkins
