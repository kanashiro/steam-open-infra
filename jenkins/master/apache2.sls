{% set master_domain = pillar['master']['config']['domain'] %}

apache2:
  pkg.installed

'a2enmod proxy':
  cmd.run

'a2enmod proxy_http':
  cmd.run

jenkins_apache_config:
  file:
    - managed
    - name: /etc/apache2/sites-available/jenkins.conf
    - source: salt://master/config/jenkins-apache2.conf.j2
    - template: jinja
    - context:
        master_domain: {{ master_domain }}
    - mode: 644
    - user: root
    - group: root

'a2ensite jenkins':
  cmd.run:
    - require:
      - file: jenkins_apache_config

apache2_daemon:
  service.running:
    - name: apache2
    - watch:
      - file: jenkins_apache_config
