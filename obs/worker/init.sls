{% set server_internal_domain = pillar['worker']['config']['server_internal_domain'] %}
{% set number_of_workers = pillar['worker']['config']['number_of_workers'] %}

obs-worker:
  pkg.installed

obsworker_config:
  file:
    - managed
    - name: /etc/default/obsworker
    - source: salt://worker/config/obsworker.j2
    - template: jinja
    - context:
        server_internal_domain: {{ server_internal_domain }}
        number_of_workers: {{ number_of_workers }}
    - mode: 644
    - user: root
    - group: root
    - require:
      - pkg: obs-worker

obsworker:
  service.running:
    - watch:
      - file: obsworker_config
