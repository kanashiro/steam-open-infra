{% set api_domain = pillar['api']['config']['api_domain'] %}

obs-server:
  pkg.installed

obs-build:
  pkg:
    - installed
    - fromrepo: stretch-backports

BSConfig_file:
  file:
    - managed
    - name: /etc/obs/BSConfig.pm
    - source: salt://server/config/BSConfig.pm.j2
    - template: jinja
    - context:
        api_domain: {{ api_domain }}
    - mode: 644
    - user: root
    - group: root
    - require:
      - pkg: obs-server
