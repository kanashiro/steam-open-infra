{% set root_passwd = pillar['db']['config']['root_passwd'] %}

mysql-server-debconf:
  debconf.set:
    - name: mysql-server
    - data:
        'mysql-server/root_password': {'type': 'password', 'value': '{{ root_passwd }}' }
        'mysql-server/root_password_again': {'type': 'password', 'value': '{{ root_passwd }}' }     
