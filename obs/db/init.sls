include:
  - db.debconf

default-mysql-server:
  pkg:
    - installed
    - require:
      - debconf: mysql-server-debconf
