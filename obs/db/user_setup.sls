{% set obs_user = pillar['db']['config']['obs_user'] %}
{% set obs_user_passwd = pillar['db']['config']['obs_user_passwd'] %}

mysql_packages:
  pkg:
    - installed
    - pkgs:
      - python-pymysql
      - python3-pymysql

mysql_user:
  mysql_user.present:
    - host: localhost
    - name: {{ obs_user }}
    - password: {{ obs_user_passwd }}
    - connection_unix_socket: /var/run/mysqld/mysqld.sock

grants_privileges_to_mysql_user:
  mysql_grants.present:
    - grant: all privileges
    - database: '*.*'
    - user: {{ obs_user }}
    - connection_unix_socket: /var/run/mysqld/mysqld.sock
