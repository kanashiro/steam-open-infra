Open Build Service - Salt formula
-----------------------------------------------

This `salt` formula automate the installation of the `open-build-service` (`OBS`) platform.

The target of this playbook are Debian Stretch VMs on Google Compute Engine (GCE). Those VMs have the `stretch-backports` repository enabled by default. Remember to allow http and https traffic during the creation of the VMs on GCE web UI.

The `open-build-platform` is divided in three components/packages mainly:

* Web UI/API (`obs-api`)
* Server (`obs-server`)
* Worker (`obs-server`)

This formula does the provision of the Web UI/API and the Server in one VM and the Worker in another one. Multiple workers was not tested yet but it should work fine.

Some config files are need to properly deploy `OBS`: `top.sls` and `pillar/top.sls` contains what will be executed in each VM, and `pillar/{api,db,worker}.sls` are customizable variables. In the repository you can find examples of all these files, all you need to do is rename those files removing the `.example` extension of their names. Detailed information of each of these files will be presented below.

The `top.sls` file contains the hostname of the VMs managed by salt minions and which salt states will be applied to each VM. This file has two sections, one for the `server` and another one for the `worker`:

```
base:
  'server':
    - common
    - db
    - db.user_setup
    - server
    - api
    - api.generate_certificate
    - api.setup

  'worker':
    - common
    - worker
```

The `server` and `worker` VMS should be managed by salt minions, and their keys should be accepted by the salt master.

The `pillar/top.sls` file describes which config file will be available to each VM:

```
base:
  'server':
    - db
    - api

  'worker':
    - worker
```

Now let's see what is needed in each config file under `pillar` directory. The first one is `pillar/api.sls`:

```
api:
  config:
    api_domain: <domain>
    certificate_dir: <absolute_path>
```

Below is the description of each variable:

* `api_domain`: The domain name used to call the API service. During the tests the public IP address of the `server` VM has been used here.
* `api_certificate_dir`: The directory where the self signed certificate stuff used by the Web UI/API will be stored. We should move to a non self signed certificate. During the tests the `/srv/certs/obs` has been used.

The next config file is `pillar/db.sls`:

```
db:
  config:
    root_passwd: <root_password>
    obs_user: <obs_user_name>
    obs_user_passwd: <obs_user_password>
```

Below is the description of each variable:

* `root_passwd`: The password of the MySQL root user.
* `obs_user`: The MySQL user that will be used to interact with the Web UI/API database.
* `obs_user_passwd`: The `obs_user`'s password.

The last config file is `pillar/worker.sls`:

```
worker:
  config:
    server_internal_domain: <server_domain>
    number_of_workers: <number_of_workers>
```

Below is the description of each variable:

* `server_internal_domain`: The internal domain name used to call the Server services. Apparently services running on non-standard ports (`5252` and `5352` for instance) are not exposed via the public IP address, and there are no configuration to allow that. During the tests the internal IP address of the `server` VM has been used here.
* `number_of_workers`: The number of workers that will be executed in the `worker` VM. During the tests `1` worker has been used.


With those files in place we can apply the salt states via the salt master. Again, at this point you should have the salt master and minions already configured, where salt master can communicate with minions without problems.

Do not forget to edit the `/etc/salt/master` config file to point to the right path. `file_roots` should point to the root of this repository and `pillar_roots` should point to the `pillar` directory inside this repository.

Finally, let's provision first the `server` and then the `worker` VMs. To do that execute the following commands to apply salt states:

```
# salt 'server' state.apply
# salt 'worker' state.apply
```

In the end you will be able to access the Web UI using `https://<api_domain>`.
