{% set obs_user = pillar['db']['config']['obs_user'] %}
{% set obs_user_passwd = pillar['db']['config']['obs_user_passwd'] %}

include:
  - api.debconf

obs-api:
  pkg:
    - installed
    - require:
      - debconf: obs-api-debconf

database_config:
  file:
    - managed
    - name: /etc/obs/api/config/database.yml
    - source: salt://api/config/database.yml.j2
    - template: jinja
    - context:
        obs_user: {{ obs_user }}
        obs_user_passwd: {{ obs_user_passwd }}
    - mode: 440
    - require:
      - pkg: obs-api
