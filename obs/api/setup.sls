{% set api_domain = pillar['api']['config']['api_domain'] %}
{% set certificate_dir= pillar['api']['config']['certificate_dir'] %}

obs_apache_config:
  file:
    - managed
    - name: /etc/apache2/sites-available/obs.conf
    - source: salt://api/config/obs-apache2.conf.j2
    - template: jinja
    - context:
        api_domain: {{ api_domain }}
        api_certificate_dir: {{ certificate_dir }}
    - mode: 644
    - user: root
    - group: root
    - require:
      - cmd: certificate_generation

'/usr/share/obs/api/script/rake-tasks.sh setup':
  cmd.run:
    - require:
      - file: obs_apache_config
