{% set api_domain = pillar['api']['config']['api_domain'] %}
{% set api_dir = '/usr/share/obs/api/public' %}
{% set certificate_dir= pillar['api']['config']['certificate_dir'] %}

openssl:
  pkg.installed

certificate_dir:
  file.directory:
    - name: {{ certificate_dir }}
    - mode: 664
    - makedirs: True

csr_generation:
  cmd.run:
    - name: >
             openssl req -new -newkey rsa:4096 -nodes
             -keyout {{ certificate_dir }}/obs.key -out {{ certificate_dir }}/obs.csr
             -subj "/CN={{ api_domain }}"
    - require:
      - pkg: openssl
      - file: certificate_dir

certificate_generation:
  cmd.run:
    - name: >
             openssl req -new -newkey rsa:4096 -nodes -x509
             -subj "/CN={{ api_domain }}"
             -keyout {{ certificate_dir }}/obs.key -out {{ certificate_dir }}/obs.crt
    - require:
      - cmd: csr_generation
