{% set config_path = pillar['runner']['config']['config_path'] %}
{% set docker_socket = pillar['runner']['config']['docker_socket'] %}
{% set base_image = pillar['runner']['config']['base_docker_image'] %}
{% set url = pillar['runner']['config']['gitlab_url'] %}
{% set domain = pillar['runner']['config']['gitlab_domain'] %}
{% set token = pillar['runner']['config']['registration_token'] %}

pull_docker_image:
  cmd.run:
    - name: docker pull gitlab/gitlab-runner:latest

tag_docker_image:
  cmd.run:
    - name: docker tag gitlab/gitlab-runner:latest gitlab-runner-valve
    - require:
      - cmd: pull_docker_image

gitlab_runner_service:
  file.managed:
    - name: /etc/systemd/system/gitlab-runner.service
    - source: salt://runner/config/gitlab-runner.service.j2
    - template: jinja
    - context:
        config_path: {{ config_path }}
        docker_socket: {{ docker_socket }}
    - user: root
    - group: root
    - mode: 640

gitlab_config_dir:
  file.directory:
    - name: /srv/gitlab-runner/config/certs
    - user: root
    - group: root
    - mode: 640
    - makedirs: True

gitlab_ci_certificate:
  file.managed:
    - name: /srv/gitlab-runner/config/certs/{{ domain }}.crt
    - source: salt://runner/config/{{ domain }}.crt
    - user: root
    - group: root
    - mode: 640
    - require:
      - file: gitlab_config_dir

gitlab-runner:
  service.running:
    - enable: True
    - require:
      - file: gitlab_runner_service
      - file: gitlab_ci_certificate

register_runner:
  cmd.run:
    - name: >
        docker run --rm --network="host" -v {{ config_path }}:/etc/gitlab-runner gitlab-runner-valve register \
              --non-interactive \
              --executor "docker" \
              --docker-image {{ base_image }} \
              --docker-network-mode "host" \
              --url "{{ url }}" \
              --registration-token "{{ token }}" \
              --description "docker-runner" \
              --tag-list "docker" \
              --run-untagged="true" \
              --locked="false"
    - require:
      - service: gitlab-runner
